package com.supryaga.loanmanager.common;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "LoanApplication")
public class LoanApplication {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue
    private Integer id;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="client_id")
    private Client client;
    @Column(name = "application_date", nullable = false)
    private Date applicationDate;
    @Column(name = "ip_address", nullable = false)
    private String ipAddress;
    @Column(name = "term", nullable = false)
    private Short term;
    @Column(name = "amount", nullable = false)
    private Long amount;

    public LoanApplication() {
    }

    public LoanApplication(Client client, Date applicationDate, String ipAddress, Short term, Long amount) {
        this.client = client;
        this.applicationDate = applicationDate;
        this.ipAddress = ipAddress;
        this.term = term;
        this.amount = amount;
    }

    public LoanApplication(int id, Client client, Date applicationDate, String ipAddress, Short term, Long amount) {
        this.id = id;
        this.client = client;
        this.applicationDate = applicationDate;
        this.ipAddress = ipAddress;
        this.term = term;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Short getTerm() {
        return term;
    }

    public void setTerm(Short term) {
        this.term = term;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
