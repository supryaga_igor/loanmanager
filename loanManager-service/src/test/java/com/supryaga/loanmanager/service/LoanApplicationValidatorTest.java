package com.supryaga.loanmanager.service;

import com.supryaga.loanmanager.common.Client;
import com.supryaga.loanmanager.common.LoanApplication;
import com.supryaga.loanmanager.dao.LoanApplicationDao;
import com.supryaga.loanmanager.service.exceptions.LoanApplicationValidationException;
import com.supryaga.loanmanager.service.validators.LoanApplicationValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:service-context.xml" })
public class LoanApplicationValidatorTest {

    @Mock
    private LoanApplicationDao loanApplicationDao;
    @InjectMocks
    @Autowired
    private LoanApplicationValidator loanApplicationValidator;
    @Value("${maxApplicationsPerDay}")
    private int maxApplicationsPerDay;
    @Value("${maxAmount}")
    private long maxAmount;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = LoanApplicationValidationException.class)
    public void testTimeBounds() throws LoanApplicationValidationException {
        when(loanApplicationDao.applicationsCountByIp("192.168.69.4")).thenReturn(1);

        Instant instant = ZonedDateTime.now(ZoneId.systemDefault()).withHour(0).
                withMinute(30).withSecond(0).toInstant();
        Date date = Date.from(instant);
        LoanApplication loanApplication = prepareLoanApplication();
        loanApplication.setApplicationDate(date);
        loanApplicationValidator.validate(loanApplication);
    }

    @Test(expected = LoanApplicationValidationException.class)
    public void testApplicationCount() throws LoanApplicationValidationException {
        when(loanApplicationDao.applicationsCountByIp("192.168.69.4")).thenReturn(maxApplicationsPerDay);

        LoanApplication loanApplication = prepareLoanApplication();
        Instant instant = ZonedDateTime.now(ZoneId.systemDefault()).withHour(16).
                withMinute(30).withSecond(0).toInstant();
        Date date = Date.from(instant);
        loanApplication.setApplicationDate(date);
        loanApplicationValidator.validate(loanApplication);

        when(loanApplicationDao.applicationsCountByIp("192.168.69.4")).thenReturn(1);

        try {
            loanApplicationValidator.validate(loanApplication);
        } catch (LoanApplicationValidationException e) {
            Assert.fail("Client can reach less than 3 applications per day");
        }
    }

    @Test(expected = LoanApplicationValidationException.class)
    public void testBothRestrictions() throws LoanApplicationValidationException {
        when(loanApplicationDao.applicationsCountByIp("192.168.69.4")).thenReturn(maxApplicationsPerDay);

        LoanApplication loanApplication = prepareLoanApplication();
        Instant instant = ZonedDateTime.now(ZoneId.systemDefault()).withHour(3).
                withMinute(30).withSecond(0).toInstant();
        Date date = Date.from(instant);
        loanApplication.setApplicationDate(date);
        loanApplicationValidator.validate(loanApplication);
    }

    private LoanApplication prepareLoanApplication() {
        Client client = new Client(1, "John", "Smith");
        return new LoanApplication(1, client, new Date(), "192.168.69.4", (short) 12, maxAmount);
    }
}
