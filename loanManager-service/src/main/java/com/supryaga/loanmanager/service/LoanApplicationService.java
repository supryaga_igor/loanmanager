package com.supryaga.loanmanager.service;

import com.supryaga.loanmanager.common.LoanApplication;
import com.supryaga.loanmanager.dao.LoanApplicationDao;
import com.supryaga.loanmanager.service.exceptions.LoanApplicationValidationException;
import com.supryaga.loanmanager.service.validators.LoanApplicationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoanApplicationService {

    @Autowired
    private LoanApplicationDao loanApplicationDao;
    @Autowired
    private LoanApplicationValidator loanApplicationValidator;

    public void add(LoanApplication loanApplication) throws LoanApplicationValidationException {
        try {
            loanApplicationValidator.validate(loanApplication);
        } catch (LoanApplicationValidationException e) {
            throw new LoanApplicationValidationException(e);
        }
        loanApplicationDao.add(loanApplication);
    }

    public LoanApplication get(int id) {
        return loanApplicationDao.get(id);
    }

}
