package com.supryaga.loanmanager.service.validators;

import com.supryaga.loanmanager.common.LoanApplication;
import com.supryaga.loanmanager.dao.LoanApplicationDao;
import com.supryaga.loanmanager.service.LoanApplicationService;
import com.supryaga.loanmanager.service.exceptions.LoanApplicationValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.*;
import java.util.Date;

@Component
public class LoanApplicationValidator {

    @Value("${minHours}")
    private int minHours;
    @Value("${maxHours}")
    private int maxHours;
    @Value("${maxAmount}")
    private int maxAmount;
    @Value("${maxApplicationsPerDay}")
    private int maxApplicationsPerDay;
    @Autowired
    private LoanApplicationDao loanApplicationDao;
    private Date lowestTimeBound;
    private Date highestTimeBound;

    public void validate(LoanApplication loanApplication) throws LoanApplicationValidationException {
        prepareTimeBounds();
        validateTimeBounds(loanApplication);
        validateApplicationsCount(loanApplication);
    }

    private void validateTimeBounds(LoanApplication loanApplication) throws LoanApplicationValidationException {
        if ((loanApplication.getApplicationDate().after(lowestTimeBound) &&
                loanApplication.getApplicationDate().before(highestTimeBound) &&
                loanApplication.getAmount() >= maxAmount)) {
            throw new LoanApplicationValidationException("Application made from " + minHours + "AM to " +
                    maxHours + "AM with maximum possible amount");
        }
    }

    private void validateApplicationsCount(LoanApplication loanApplication) throws LoanApplicationValidationException {
        if (loanApplicationDao.applicationsCountByIp(loanApplication.getIpAddress()) >= maxApplicationsPerDay) {
            throw new LoanApplicationValidationException("Applications count per day must be less than " + maxApplicationsPerDay);
        }
    }

    private void prepareTimeBounds() {
        Instant lowestBoundInstant = ZonedDateTime.now(ZoneId.systemDefault()).withHour(minHours).
                withMinute(0).withSecond(0).toInstant();
        lowestTimeBound = Date.from(lowestBoundInstant);

        Instant highestBoundInstant = ZonedDateTime.now(ZoneId.systemDefault()).withHour(maxHours).
                withMinute(0).withSecond(0).toInstant();
        highestTimeBound = Date.from(highestBoundInstant);
    }
}
