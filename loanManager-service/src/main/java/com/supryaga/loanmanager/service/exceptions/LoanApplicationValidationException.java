package com.supryaga.loanmanager.service.exceptions;

public class LoanApplicationValidationException extends Exception {

    public LoanApplicationValidationException(String message) {
        super(message);
    }

    public LoanApplicationValidationException(Throwable cause) {
        super(cause);
    }

    public LoanApplicationValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
