package com.supryaga.loanmanager.service;

import com.supryaga.loanmanager.common.Client;
import com.supryaga.loanmanager.dao.ClientDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientService {

    @Autowired
    private ClientDao clientDao;

    public void add(Client obj) {
        clientDao.add(obj);
    }

}
