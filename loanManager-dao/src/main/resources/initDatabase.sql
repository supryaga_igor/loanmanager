create database loanmanager;
use loanmanager;
create TABLE Client(id INT PRIMARY KEY auto_increment,
  name VARCHAR(255) NOT NULL, surname VARCHAR(255));

create TABLE LoanApplication(id INT PRIMARY KEY auto_increment, client_id INT, application_date TIMESTAMP,
                             ip_address VARCHAR(15), term SMALLINT, amount BIGINT );

ALTER TABLE loanmanager.LoanApplication ADD CONSTRAINT fk_loanapp_2_client FOREIGN KEY(client_id) REFERENCES loanmanager.Client(id);
ALTER TABLE loanmanager.Client ADD UNIQUE(name, surname);
