package com.supryaga.loanmanager.dao.utils;

import com.supryaga.loanmanager.dao.exceptions.LoanManagerDaoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    private static final SessionFactory sessionFactory;

    static {
        try {
            Configuration configuration = new Configuration().configure();
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                    applySettings(configuration.getProperties());
            sessionFactory = configuration.buildSessionFactory(builder.build());
        } catch (Throwable e) {
            throw new RuntimeException("Cant initialize session factory", e);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void close(Session session) {
        if (session != null && session.isConnected()) {
            try {
                session.close();
            } catch (HibernateException e) {
                throw new LoanManagerDaoException(e.getMessage(), e);
            }
        }
    }
}
