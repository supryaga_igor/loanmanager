package com.supryaga.loanmanager.dao;

import com.supryaga.loanmanager.common.Client;
import com.supryaga.loanmanager.dao.utils.HibernateUtil;
import org.springframework.stereotype.Component;

@Component
public class ClientDao extends AbstractDao {

    private static final String SELECT_BY_NAME_QUERY = "FROM Client WHERE name = :name AND surname = :surname ";

    public void add(Client obj) {
        saveorUpdate(obj);
    }

    public Client getByNameAndSurname(Client obj) {
        initSessionAndBeginTransaction();
        query = session.createQuery(SELECT_BY_NAME_QUERY);
        query.setString("name", obj.getName());
        query.setString("surname", obj.getSurname());
        Client client = (Client) query.uniqueResult();
        transaction.commit();
        HibernateUtil.close(session);

        return client;
    }
}
