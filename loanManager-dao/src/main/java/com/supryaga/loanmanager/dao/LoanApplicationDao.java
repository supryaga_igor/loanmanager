package com.supryaga.loanmanager.dao;

import com.supryaga.loanmanager.common.Client;
import com.supryaga.loanmanager.common.LoanApplication;
import com.supryaga.loanmanager.dao.utils.HibernateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoanApplicationDao extends AbstractDao {

    private static final String APPLICATIONS_COUNT_BY_IP = "SELECT COUNT(*) FROM LoanApplication WHERE ip_address = :ipAddress";
    private static final String GET_BY_ID = "FROM LoanApplication WHERE id = :id";
    @Autowired
    private ClientDao clientDao;

    public void add(LoanApplication obj) {
        Client client = clientDao.getByNameAndSurname(obj.getClient());
        if (client != null) {
            obj.setClient(client);
        }

        saveorUpdate(obj);
    }

    public int applicationsCountByIp(String ipAddress) {
        initSessionAndBeginTransaction();
        query = session.createQuery(APPLICATIONS_COUNT_BY_IP);
        query.setString("ipAddress", ipAddress);
        Long count = (Long) query.uniqueResult();
        transaction.commit();
        HibernateUtil.close(session);

        return count.intValue();
    }

    public LoanApplication get(int id) {
        initSessionAndBeginTransaction();
        query = session.createQuery(GET_BY_ID);
        query.setInteger("id", id);
        LoanApplication result = (LoanApplication) query.uniqueResult();
        transaction.commit();
        HibernateUtil.close(session);

        return result;
    }
}
