package com.supryaga.loanmanager.dao;


import com.supryaga.loanmanager.dao.utils.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class AbstractDao {

    protected Session session;
    protected Transaction transaction;
    protected Query query;

    public void initSessionAndBeginTransaction() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        transaction = session.beginTransaction();
    }

    public void saveorUpdate(Object obj) {
        try {
            initSessionAndBeginTransaction();
            session.saveOrUpdate(obj);
            transaction.commit();
        } finally {
            HibernateUtil.close(session);
        }
    }
}
