package com.supryaga.loanmanager.dao.exceptions;

public class LoanManagerDaoException extends RuntimeException {

    public LoanManagerDaoException() {
    }

    public LoanManagerDaoException(String message) {
        super(message);
    }

    public LoanManagerDaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
