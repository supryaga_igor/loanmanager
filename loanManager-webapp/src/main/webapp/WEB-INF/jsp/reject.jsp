<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/header.jsp"/>
<div class="alert alert-danger">
  <strong>Reject!</strong> Sorry, try again later.
</div>
</body>
</html>
