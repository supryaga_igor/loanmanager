<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/header.jsp"/>
<div class="alert alert-success">
  <strong>Application info:</strong>
</div>
<table class="table">
  <thead>
  <tr>
    <th>Application ID</th>
    <th>Name</th>
    <th>Surname</th>
    <th>Application date</th>
    <th>IP Address</th>
    <th>Term</th>
    <th>Amount</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <th>${loanApplication.id}</th>
    <th>${loanApplication.client.name}</th>
    <th>${loanApplication.client.surname}</th>
    <th>${loanApplication.applicationDate}</th>
    <th>${loanApplication.ipAddress}</th>
    <th>${loanApplication.term}</th>
    <th>${loanApplication.amount}</th>
  </tr>
  </tbody>
</table>
</body>
</html>
