<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/header.jsp"/>
<div class="container">
    <div class="centered-text text-info lato-normal font-weight-300 font-size-36">Apply for loan</div>
    <form data-toggle="validator" role="form" action="<c:url value="/checkApplication"/>" method="post">
        <div class="form-group">
            <label class="control-label">Name <span class="required-field-asterisk">*</span></label>
            <input type="text" class="form-control" name="name" required>
        </div>
        <div class="form-group">
            <label>Surname <span class="required-field-asterisk">*</span></label>
            <input type="text" class="form-control" name="surname" required>
        </div>
        <div class="form-group">
            <label>Amount <span class="required-field-asterisk">*</span></label>
            <input type="number" min="100" max="10000" class="form-control" name="amount" required>
        </div>
        <div class="form-group">
            <label>Term (months) <span class="required-field-asterisk">*</span></label>
            <input type="number" min="3" max="36" class="form-control" name="term" required>
        </div>
        <div class="form-group">
            <span class="text-muted"><em><span class="required-field-asterisk">*</span> Indicates required field</em></span>
        </div>
        <div class="form-group">
            <div class="centered-div">
                <input type="submit" class="btn btn-primary full-width-element lato-normal font-weight-300 font-size-20" name="update" value="Submit" />
            </div>
        </div>

    </form>
</div>
</body>
</html>