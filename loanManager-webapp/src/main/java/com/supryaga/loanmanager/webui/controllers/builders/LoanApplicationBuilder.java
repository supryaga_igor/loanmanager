package com.supryaga.loanmanager.webui.controllers.builders;

import com.supryaga.loanmanager.common.Client;
import com.supryaga.loanmanager.common.LoanApplication;
import javax.servlet.http.HttpServletRequest;
import java.time.*;
import java.util.Date;

public class LoanApplicationBuilder {

    private HttpServletRequest request;

    public LoanApplicationBuilder(HttpServletRequest request) {
        this.request = request;
    }

    public LoanApplication build() {
        Client client = new ClientBuilder(request).build();
        Instant now = ZonedDateTime.now(ZoneId.systemDefault()).toInstant();
        Date applicationDate = Date.from(now);

        return new LoanApplication(client, applicationDate,
                    request.getRemoteAddr(), Short.valueOf(request.getParameter("term")),
                    Long.valueOf(request.getParameter("amount")));
    }

}