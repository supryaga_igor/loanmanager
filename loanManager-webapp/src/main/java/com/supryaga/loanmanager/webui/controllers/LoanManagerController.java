package com.supryaga.loanmanager.webui.controllers;

import com.supryaga.loanmanager.common.LoanApplication;
import com.supryaga.loanmanager.service.LoanApplicationService;
import com.supryaga.loanmanager.service.exceptions.LoanApplicationValidationException;
import com.supryaga.loanmanager.webui.controllers.builders.LoanApplicationBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;

@Controller
public class LoanManagerController {

    @Autowired
    private LoanApplicationService loanApplicationService;


    @RequestMapping("/")
    public ModelAndView uploadImages() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "displayApplicationInfo/{applicationId}", method = RequestMethod.GET)
    public ModelAndView displayLoanApplication(@PathVariable int applicationId, Model model) {
        LoanApplication loanApplication = loanApplicationService.get(applicationId);
        if (loanApplication != null) {
            model.addAttribute("loanApplication", loanApplication);
        } else {
            return new ModelAndView("applicationNotFound");
        }

        return new ModelAndView("displayLoanApplicationInfo");
    }

    @RequestMapping(value = "/checkApplication", method = RequestMethod.POST)
    public ModelAndView checkApplication(HttpServletRequest request, Model model) {
        LoanApplication loanApplication = new LoanApplicationBuilder(request).build();
        try {
            loanApplicationService.add(loanApplication);
        } catch (LoanApplicationValidationException e) {
            return new ModelAndView("reject");
        }
        model.addAttribute("loanApplication", loanApplication);

        return new ModelAndView("displayLoanApplicationInfo");
    }
}
