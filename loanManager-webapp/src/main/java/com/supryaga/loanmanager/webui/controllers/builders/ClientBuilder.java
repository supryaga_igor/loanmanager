package com.supryaga.loanmanager.webui.controllers.builders;

import com.supryaga.loanmanager.common.Client;
import javax.servlet.http.HttpServletRequest;

public class ClientBuilder {

    private HttpServletRequest request;

    public ClientBuilder(HttpServletRequest request) {
        this.request = request;
    }

    public Client build() {
        return new Client(request.getParameter("name"), request.getParameter("surname"));
    }

}
